const http = require("http");
const port = 4000;

http.createServer((req,res)=>{
    if(req.url=="/"&&req.method=="GET"){
        res.write("Welcome to booking system");
    }else if(req.url=="/profile"&&req.method=="GET"){
        res.write("Welcome to your profile");
    }else if(req.url=="/courses"&&req.method=="GET"){
        res.write("Here's our courses available");
    }else if(req.url=="/addCourse"&&req.method=="POST"){
        res.write("Add courses to our resources");
    }else if(req.url=="/updateCourse"&&req.method=="PUT"){
        res.write("Update a course to our resources");
    }else if(req.url=="/archiveCourse"&&req.method=="DELETE"){
        res.write("Archive courses to our resources");
    }else{
        res.writeHead(404,{"Content-Type":"text/html"})
        res.write("Page cannot be found");
    }
    res.end();

}).listen(port,()=>{
    console.log("connected to port",port)
})